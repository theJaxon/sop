stack_sum_list = []

# Was used in the initial solution but no longer used
def stack_sum(stack):
    total = 0
    for item in stack:
        total = total + item
    return total


def equalStacks(stack_one, stack_two, stack_three):
    target_value = min(sum(stack_one), sum(stack_two), sum(stack_three))

    # Keep looping until the condition of having 3 equal stacks is satisfied
    while sum(stack_one) != sum(stack_two) != sum(stack_three):
        # We can assume that the stack height will NEVER exceed the total sum of the smallest stack
        # So given the input stack_one[3, 2, 1, 1, 1] , stack_two[4, 3, 2], stack_three[1, 1, 4, 1]
        # total s_one=8, s_two=9, s_three=7
        # We can conclude it will at minimum be 7, it can be less than that but not above the lowest stack so initially its our target number and based on it we reduce other stacks
        if sum(stack_one) > target_value:
            stack_one.pop(0)

        if sum(stack_two) > target_value:
            stack_two.pop(0)

        if sum(stack_three) > target_value:
            stack_three.pop(0)

        stack_sum_list = [sum(stack_one), sum(stack_two), sum(stack_three)]

        # Used for checking whether we have 2 stacks of the same high value or not because if it's the case, when popping we need to pop the stack with the smallest first item
        biggest_list = max(stack_sum_list)

        # Checks for stacks with same big high values, if so pops the smallest unit of the big stack
        if stack_sum_list.count(biggest_list) > 1:
            if sum(stack_one) == sum(stack_two):
                # Pop the smallest item if the 2 lists are equal
                if stack_one[0] > stack_two[0]:
                    stack_two.pop(0)

            elif sum(stack_two) == sum(stack_three):
                if stack_two[0] > stack_three[0]:
                    stack_three.pop(0)

            elif sum(stack_three) == sum(stack_one):
                if stack_three[0] > stack_one[0]:
                    stack_one.pop(0)

        # If there are no 2 equal big stacks then we check what's the biggest stack and pop its first element
        else:
            if sum(stack_one) == biggest_list:
                stack_one.pop(0)

            elif sum(stack_two) == biggest_list:
                stack_two.pop(0)

            elif sum(stack_three) == biggest_list:
                stack_three.pop(0)

    print(stack_one, stack_two, stack_three)

    # Since all are same height, sum of any of them should print correct height
    print(f"Stack height is {sum(stack_one)}\n")

    # Remaining is the code commented from when i started working on this task and I initially chose a complex approach that never worked
    # That was a dictionary with a tuple as key and sum of elements as value and I faced many issues as tuples are immutable and i can't have a list inside the dictionary so In the end i restarted the whole thing with a list

    # # Working on first stack comparison
    # if sum(stack_one) >= sum(stack_two):
    #     if sum(stack_one) >= sum(stack_three):
    #         stack_one = stack_one.pop()

    # # Working on second stack comparison
    # if sum(stack_two) >= sum(stack_one):
    #     if sum(stack_two) >= sum(stack_three):
    #         stack_two = stack_two.pop()

    # # Working on third stack comparison
    # if sum(stack_three) >= sum(stack_one):
    #     if sum(stack_three) >= sum(stack_two):
    #         stack_three = stack_three.pop()

    # target_number = min(list(stack_dict.values()))
    # # print(f"trying {target_number} as a target number")
    # # print(stack_dict)
    # # print(sorted(stack_dict.items(), key=lambda x: (x[1], x[0]), reverse=True))

    # # Sort dictionary elements
    # # https://stackoverflow.com/a/7742808
    # sorted_stack_dict = dict(
    #     sorted(stack_dict.items(), key=lambda x: (x[1], x[0]), reverse=True)
    # )
    # # print(sorted_stack_dict)

    # for key, value in sorted_stack_dict.items():
    #     if value > target_number:
    #         keys_list = list(key)
    #         keys_list.pop(0)
    #         key_tuple = tuple(keys_list)
    #         print(key_tuple)

    # if (
    #     list(sorted_stack_dict.values())[0]
    #     == list(sorted_stack_dict.values())[1]
    #     == list(sorted_stack_dict.values())[2]
    # ):
    #     print("not eqqu")

    # for items_tuple, sum_of_items in sorted_stack_dict.items():
    #     if sum_of_items > target_number:
    #         items_list = list(items_tuple)
    #         items_list.pop(0)
    #         items_tuple = tuple(items_list)
    #         print(items_tuple)

    #     if sum_of_items <= target_number:
    #         print(sorted_stack_dict)
    #         print(list(sorted_stack_dict.values())[0])

    # if(stack_sum(sorted_stack_dict[0]))

    # for item in stack_dict.keys():
    #     for key in item:
    #         print(key)

    # biggest_stack = max(stack_dict.values())
    # for key, value in stack_dict.items():
    #     if biggest_stack == value:
    #         print(key, value)

    #     # if stack_sum(stack_one) == stack_sum(stack_two) == stack_sum(stack_three):
    #     #     print(f"succeeded with {target_number}")

    #     # else:
    #     # Detect whether the biggest stack is only 1 or multiple
    #     biggest_stack = max(stack_dict.values())
    #     for key, value in stack_dict.items():
    #         if biggest_stack == value:
    #             biggest_stack_counter = biggest_stack_counter + 1

    #     for key, value in stack_dict.items():
    #         # If there's only 1 big stack then pop its first element
    #         if biggest_stack_counter == 1:
    #             if value == biggest_stack:
    #                 big_stack_list = list(key)
    #                 big_stack_list.pop()

    #                 print(big_stack_list)

    # if item == biggest_stack:
    # stack_dict[item]

    # Choose the biggest stack in size as the target so that we can remove its first element and check
    # Edge case to handle is if there are 2 big stacks same in size, here we select the one with the lowest first element

    # Next Assumption is, check if the first element was removed from the largest stack, will it be equal to the lowest ? if so proceed to next element and do the same
    # If not then check if it became lower than the minimum stack element
    # stack_sum_list.append(stack_sum(stack_one))
    # stack_sum_list.append(stack_sum(stack_two))
    # stack_sum_list.append(stack_sum(stack_three))

    # We can assume this might be the correct number initially
    # min_stack_sum = min(stack_sum_list)
    # max_stack_sum = max(stack_sum_list)


equalStacks([1, 2, 1, 1], [1, 1, 2], [1, 1])
equalStacks([3, 2, 1, 1, 1], [4, 3, 2], [1, 1, 4, 1])
# print(min(stack_sum_list))
